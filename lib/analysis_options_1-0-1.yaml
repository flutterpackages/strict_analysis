# Created on 20230430T160306
#
# All rules: https://dart-lang.github.io/linter/lints/index.html
#
# - incompatible lints are included but commented
# - removed lints are included but commented
# - deprecated lints are included but commented
# - unreleased lints are included but commented
# - experimental lints are not included.

linter:
  rules:
    ## Error rules (Possible coding errors)

    # This is incompatible with "prefer_relative_imports"
    # always_use_package_imports:

    #+i error
    avoid_dynamic_calls: true

    #+i warning
    avoid_empty_else: true

    #+i info
    avoid_print: true

    #+i info
    avoid_relative_lib_imports: true

    #! DEPRECATED
    # See https://dart-lang.github.io/linter/lints/avoid_returning_null_for_future.html
    # avoid_returning_null_for_future: true

    #+i warning
    avoid_slow_async_io: true

    #+i info
    avoid_type_to_string: true

    #+i error
    avoid_types_as_parameter_names: true

    #+i error
    avoid_web_libraries_in_flutter: true

    #+i warning
    cancel_subscriptions: true

    #+i warning
    close_sinks: true

    #+i warning
    collection_methods_unrelated_type: true

    #+i info
    comment_references: true

    #+i warning
    control_flow_in_finally: true

    # Unreleased
    # See https://dart-lang.github.io/linter/lints/deprecated_member_use_from_same_package.html
    #+i info
    # deprecated_member_use_from_same_package: true

    #+i info
    diagnostic_describe_all_properties: true

    #+i warning
    discarded_futures: true

    #+i error
    empty_statements: true

    #+i warning
    hash_and_equals: true

    #! REMOVED
    # See https://dart-lang.github.io/linter/lints/invariant_booleans.html
    #+i error
    # invariant_booleans: true

    # This has been replaced by "collection_methods_unrelated_type".
    # iterable_contains_unrelated_type: true

    #+i info
    list_remove_unrelated_type: true

    #+i error
    literal_only_boolean_expressions: true

    # Disabled
    no_adjacent_strings_in_list: false

    #+i error
    no_duplicate_case_values: true

    #+i warning
    no_logic_in_create_state: true

    #+i info
    prefer_relative_imports: true

    #+i info
    prefer_void_to_null: true

    #+i warning
    test_types_in_equals: true

    #+i error
    throw_in_finally: true

    #+i error
    unnecessary_statements: true

    #+i error
    unrelated_type_equality_checks: true

    #+i error
    unsafe_html: true

    #+i info
    use_key_in_widget_constructors: true

    #+i error
    valid_regexps: true

    ## Style rules (Matters of style, largely derived from the official Dart Style Guide)

    #+i error
    always_declare_return_types: true

    # Disabled
    always_put_control_body_on_new_line: false

    #+i info
    always_put_required_named_parameters_first: true

    #! DEPRECATED
    # See https://dart-lang.github.io/linter/lints/always_require_non_null_named_parameters.html
    # always_require_non_null_named_parameters: true

    # Disabled
    always_specify_types: false

    #+i info
    annotate_overrides: true

    # Disabled
    avoid_annotating_with_dynamic: false

    # Removed since Dart 2.12.0 we support Dart 2.18.0 and upwards,
    # so we must not include this lint.
    # avoid_as: true

    #+i info
    avoid_bool_literals_in_conditional_expressions: true

    #+i warning
    avoid_catches_without_on_clauses: true

    #+i warning
    avoid_catching_errors: true

    #+i info
    avoid_classes_with_only_static_members: true

    # Disabled
    avoid_double_and_int_checks: false

    # Disabled
    avoid_equals_and_hash_code_on_mutable_classes: false

    #+i info
    avoid_escaping_inner_quotes: true

    #+i info
    avoid_field_initializers_in_const_classes: true

    #+i info
    avoid_final_parameters: true

    #+i info
    avoid_function_literals_in_foreach_calls: false

    #+i warning
    avoid_implementing_value_types: true

    #+i info
    avoid_init_to_null: true

    # Disabled
    avoid_js_rounded_ints: false

    #+i warning
    avoid_multiple_declarations_per_line: true

    #+i info
    avoid_null_checks_in_equality_operators: true

    # Disabled
    avoid_positional_boolean_parameters: false

    #+i info
    avoid_private_typedef_functions: true

    # Disabled
    avoid_redundant_argument_values: false

    #+i info
    avoid_renaming_method_parameters: true

    #+i warning
    avoid_return_types_on_setters: true

    #! DEPRECATED
    # See https://dart-lang.github.io/linter/lints/avoid_returning_null.html
    # avoid_returning_null: true

    #+i info
    avoid_returning_null_for_void: true

    #+i info
    avoid_returning_this: true

    # Disabled
    avoid_setters_without_getters: false

    #+i info
    avoid_shadowing_type_parameters: true

    #+i info
    avoid_single_cascade_in_expression_statements: true

    #+i info
    avoid_types_on_closure_parameters: true

    #+i info
    avoid_unnecessary_containers: true

    #+i info
    avoid_unused_constructor_parameters: true

    #+i warning
    avoid_void_async: true

    #+i info
    await_only_futures: true

    #+i info
    camel_case_extensions: true

    #+i info
    camel_case_types: true

    # Disabled
    cascade_invocations: false

    #+i warning
    cast_nullable_to_non_nullable: true

    #+i info
    combinators_ordering: true

    #+i error
    conditional_uri_does_not_exist: true

    #+i info
    constant_identifier_names: true

    # Disabled
    curly_braces_in_flow_control_structures: false

    #+i info
    dangling_library_doc_comments: true

    #+i info
    deprecated_consistency: true

    #+i info
    directives_ordering: true

    #+i info
    do_not_use_environment: true

    #+i warning
    empty_catches: true

    #+i info
    empty_constructor_bodies: true

    # Removed since Dart 2.12.0 we support Dart 2.18.0 and upwards,
    # so we must not include this lint.
    # enable_null_safety

    #+i info
    eol_at_end_of_file: true

    #+i warning
    exhaustive_cases: true

    #+i info
    file_names: true

    #+i info
    flutter_style_todos: true

    #+i warning
    implementation_imports: true

    # Disabled
    implicit_call_tearoffs: false

    # Disabled
    join_return_with_assignment: false

    #+i info
    leading_newlines_in_multiline_strings: true

    #+i info
    library_annotations: true

    #+i info
    library_names: true

    #+i info
    library_prefixes: true

    # Disabled
    library_private_types_in_public_api: false

    #+i info
    lines_longer_than_80_chars: true

    # Linter for this is unreleased
    # See https://dart-lang.github.io/linter/lints/matching_super_parameters.html
    #+i info
    # matching_super_parameters: true

    #+i info
    missing_whitespace_between_adjacent_strings: true

    #+i info
    no_leading_underscores_for_library_prefixes: true

    #+i info
    no_leading_underscores_for_local_identifiers: true

    # Linter for this is unreleased
    # See https://dart-lang.github.io/linter/lints/no_literal_bool_comparisons.html
    #+i info
    # no_literal_bool_comparisons: true

    #+i info
    no_runtimeType_toString: true

    #+i info
    non_constant_identifier_names: true

    #+i info
    noop_primitive_operations: true

    #+i warning
    null_check_on_nullable_type_parameter: true

    #+i info
    null_closures: true

    #+i info
    omit_local_variable_types: true

    #+i info
    one_member_abstracts: true

    #+i warning
    only_throw_errors: true

    # Disabled
    overridden_fields: false

    #+i info
    package_api_docs: true

    #+i info
    package_prefixed_library_names: true

    #+i info
    parameter_assignments: true

    #+i info
    prefer_adjacent_string_concatenation: true

    #+i info
    prefer_asserts_in_initializer_lists: true

    # Disabled
    prefer_asserts_with_message: false

    #! Removed
    # See https://dart-lang.github.io/linter/lints/prefer_bool_in_asserts.html
    #+i error
    # prefer_bool_in_asserts: true

    #+i info
    prefer_collection_literals: true

    #+i info
    prefer_conditional_assignment: true

    #+i info
    prefer_const_constructors: true

    #+i info
    prefer_const_constructors_in_immutables: true

    #+i info
    prefer_const_declarations: true

    #+i info
    prefer_const_literals_to_create_immutables: true

    #+i info
    prefer_constructors_over_static_methods: true

    #+i info
    prefer_contains: true

    #+i info
    prefer_double_quotes: true

    # Removed in Dart 3.0.0, but we support Dart 2.18.0 and upwards,
    # so we still include this lint.
    #+i info
    prefer_equal_for_default_values: true

    # Disabled
    prefer_expression_function_bodies: false

    #+i info
    prefer_final_fields: true

    #+i info
    prefer_final_in_for_each: true

    #+i info
    prefer_final_locals: true

    # This is incompatible with "avoid_final_parameters"
    # prefer_final_parameters:

    # Disabled
    prefer_for_elements_to_map_fromIterable: false

    #+i info
    prefer_foreach: true

    #+i info
    prefer_function_declarations_over_variables: true

    #+i info
    prefer_generic_function_type_aliases: true

    #+i info
    prefer_if_elements_to_conditional_expressions: true

    #+i info
    prefer_if_null_operators: true

    #+i info
    prefer_initializing_formals: true

    #+i info
    prefer_inlined_adds: true

    #+i info
    prefer_int_literals: true

    #+i info
    prefer_interpolation_to_compose_strings: true

    #+i info
    prefer_is_empty: true

    #+i info
    prefer_is_not_empty: true

    #+i info
    prefer_is_not_operator: true

    #+i info
    prefer_iterable_whereType: true

    #+i info
    prefer_mixin: true

    #+i info
    prefer_null_aware_method_calls: true

    #+i info
    prefer_null_aware_operators: true

    # This is incompatible with "prefer_double_quotes"
    # prefer_single_quotes: false

    #+i info
    prefer_spread_collections: true

    #+i warning
    prefer_typing_uninitialized_variables: true

    #+i info
    provide_deprecation_message: true

    # Disabled
    public_member_api_docs: false

    #+i warning
    recursive_getters: true

    #+i info
    require_trailing_commas: true

    #+i info
    sized_box_for_whitespace: true

    #+i info
    sized_box_shrink_expand: true

    #+i info
    slash_for_doc_comments: true

    #+i info
    sort_child_properties_last: true

    #+i info
    sort_constructors_first: true

    #+i info
    sort_unnamed_constructors_first: true

    #! REMOVED
    # See https://dart-lang.github.io/linter/lints/super_goes_last.html
    #+i info
    # super_goes_last: true

    #+i info
    tighten_type_of_initializing_formals: true

    #+i warning
    type_annotate_public_apis: true

    #+i info
    type_init_formals: true

    # Don't really understand what this one does?
    # type_literal_in_constant_pattern

    #+i warning
    unawaited_futures: true

    #+i info
    unnecessary_await_in_return: true

    #+i info
    unnecessary_brace_in_string_interps: true

    # Unreleased
    # See https://dart-lang.github.io/linter/lints/unnecessary_breaks.html
    # Disabled
    # unnecessary_breaks: false

    #+i info
    unnecessary_const: true

    #+i info
    unnecessary_constructor_name: true

    # Incompatible with: prefer_final_locals, prefer_final_parameters.
    # unnecessary_final: false

    #+i info
    unnecessary_getters_setters: true

    #+i info
    unnecessary_lambdas: true

    #+i info
    unnecessary_late: true

    #+i info
    unnecessary_library_directive: true

    #+i info
    unnecessary_new: true

    #+i info
    unnecessary_null_aware_assignments: true

    #+i info
    unnecessary_null_aware_operator_on_extension_on_nullable: true

    #+i info
    unnecessary_null_in_if_null_operators: true

    #+i info
    unnecessary_nullable_for_final_variable_declarations: true

    #+i info
    unnecessary_overrides: true

    #+i info
    unnecessary_parenthesis: true

    #+i info
    unnecessary_raw_strings: true

    #+i info
    unnecessary_string_escapes: true

    #+i info
    unnecessary_string_interpolations: true

    #+i info
    unnecessary_this: true

    #+i info
    unnecessary_to_list_in_spreads: true

    #+i info
    use_colored_box: true

    #+i info
    use_decorated_box: true

    #+i info
    use_enums: true

    #+i info
    use_full_hex_values_for_flutter_colors: true

    #+i info
    use_function_type_syntax_for_parameters: true

    # Disabled
    use_if_null_to_convert_nulls_to_bools: false

    #+i info
    use_is_even_rather_than_modulo: true

    #+i info
    use_named_constants: true

    #+i info
    use_raw_strings: true

    #+i info
    use_rethrow_when_possible: true

    #+i info
    use_setters_to_change_properties: true

    #+i info
    use_string_buffers: true

    #+i info
    use_string_in_part_of_directives: true

    #+i info
    use_test_throws_matchers: true

    #+i info
    use_to_and_as_if_applicable: true

    #+i info
    void_checks: true

    ## Pub rules (Pub-related rules)

    #+i warning
    depend_on_referenced_packages: true

    #+i info
    package_names: true

    #+i info
    secure_pubspec_urls: true

    # Disabled
    sort_pub_dependencies: false

analyzer:
  exclude:
    - .dart_tool/**
    # Generated source files
    - lib/**.g.dart
    # Outpt from flutter build
    - build/**
    # Output from https://pub.dev/packages/freezed
    - lib/**.freezed.dart
    # Output from https://pub.dev/packages/protobuf
    - lib/**.pb*.dart

  language:
    # See https://dart.dev/guides/language/analysis-options#enabling-additional-type-checks
    strict-casts: false
    strict-inference: true
    strict-raw-types: false

  # Set the severity level of errors
  # Either:
  #   - info
  #   - warning
  #   - error
  errors: 
    todo: info
    dead_code: info
    invalid_assignment: error
    missing_required_param: error
    missing_return: error
    import_of_legacy_library_into_null_safe: error
    invalid_use_of_visible_for_testing_member: warning
    avoid_empty_else: warning
    avoid_print: info
    avoid_relative_lib_imports: info
    avoid_slow_async_io: warning
    avoid_type_to_string: info
    avoid_types_as_parameter_names: error
    avoid_web_libraries_in_flutter: error
    cancel_subscriptions: warning
    close_sinks: warning
    collection_methods_unrelated_type: warning
    comment_references: info
    control_flow_in_finally: warning
    diagnostic_describe_all_properties: info
    discarded_futures: warning
    empty_statements: error
    hash_and_equals: warning
    list_remove_unrelated_type: info
    literal_only_boolean_expressions: error
    no_duplicate_case_values: error
    no_logic_in_create_state: warning
    prefer_relative_imports: info
    prefer_void_to_null: info
    test_types_in_equals: warning
    throw_in_finally: error
    unnecessary_statements: error
    unrelated_type_equality_checks: error
    unsafe_html: error
    use_key_in_widget_constructors: info
    valid_regexps: error
    always_declare_return_types: error
    always_put_required_named_parameters_first: info
    annotate_overrides: info
    avoid_bool_literals_in_conditional_expressions: info
    avoid_catches_without_on_clauses: warning
    avoid_catching_errors: warning
    avoid_classes_with_only_static_members: info
    avoid_escaping_inner_quotes: info
    avoid_field_initializers_in_const_classes: info
    avoid_final_parameters: info
    avoid_implementing_value_types: warning
    avoid_init_to_null: info
    avoid_multiple_declarations_per_line: warning
    avoid_null_checks_in_equality_operators: info
    avoid_private_typedef_functions: info
    avoid_renaming_method_parameters: info
    avoid_return_types_on_setters: warning
    avoid_returning_null_for_void: info
    avoid_returning_this: info
    avoid_shadowing_type_parameters: info
    avoid_single_cascade_in_expression_statements: info
    avoid_types_on_closure_parameters: info
    avoid_unnecessary_containers: info
    avoid_unused_constructor_parameters: info
    avoid_void_async: warning
    await_only_futures: info
    camel_case_extensions: info
    camel_case_types: info
    cast_nullable_to_non_nullable: warning
    combinators_ordering: info
    conditional_uri_does_not_exist: error
    constant_identifier_names: info
    dangling_library_doc_comments: info
    deprecated_consistency: info
    directives_ordering: info
    do_not_use_environment: info
    empty_catches: warning
    empty_constructor_bodies: info
    eol_at_end_of_file: info
    exhaustive_cases: warning
    file_names: info
    flutter_style_todos: info
    implementation_imports: warning
    leading_newlines_in_multiline_strings: info
    library_annotations: info
    library_names: info
    library_prefixes: info
    lines_longer_than_80_chars: info
    missing_whitespace_between_adjacent_strings: info
    no_leading_underscores_for_library_prefixes: info
    no_leading_underscores_for_local_identifiers: info
    no_runtimeType_toString: info
    non_constant_identifier_names: info
    noop_primitive_operations: info
    null_check_on_nullable_type_parameter: warning
    null_closures: info
    omit_local_variable_types: info
    one_member_abstracts: info
    only_throw_errors: warning
    package_api_docs: info
    package_prefixed_library_names: info
    parameter_assignments: info
    prefer_adjacent_string_concatenation: info
    prefer_asserts_in_initializer_lists: info
    prefer_collection_literals: info
    prefer_conditional_assignment: info
    prefer_const_constructors: info
    prefer_const_constructors_in_immutables: info
    prefer_const_declarations: info
    prefer_const_literals_to_create_immutables: info
    prefer_constructors_over_static_methods: info
    prefer_contains: info
    prefer_double_quotes: info
    prefer_equal_for_default_values: info
    prefer_final_fields: info
    prefer_final_in_for_each: info
    prefer_final_locals: info
    prefer_foreach: info
    prefer_function_declarations_over_variables: info
    prefer_generic_function_type_aliases: info
    prefer_if_elements_to_conditional_expressions: info
    prefer_if_null_operators: info
    prefer_initializing_formals: info
    prefer_inlined_adds: info
    prefer_int_literals: info
    prefer_interpolation_to_compose_strings: info
    prefer_is_empty: info
    prefer_is_not_empty: info
    prefer_is_not_operator: info
    prefer_iterable_whereType: info
    prefer_mixin: info
    prefer_null_aware_method_calls: info
    prefer_null_aware_operators: info
    prefer_spread_collections: info
    prefer_typing_uninitialized_variables: warning
    provide_deprecation_message: info
    recursive_getters: warning
    require_trailing_commas: info
    sized_box_for_whitespace: info
    sized_box_shrink_expand: info
    slash_for_doc_comments: info
    sort_child_properties_last: info
    sort_constructors_first: info
    sort_unnamed_constructors_first: info
    tighten_type_of_initializing_formals: info
    type_annotate_public_apis: warning
    type_init_formals: info
    unawaited_futures: warning
    unnecessary_await_in_return: info
    unnecessary_brace_in_string_interps: info
    unnecessary_const: info
    unnecessary_constructor_name: info
    unnecessary_getters_setters: info
    unnecessary_lambdas: info
    unnecessary_late: info
    unnecessary_library_directive: info
    unnecessary_new: info
    unnecessary_null_aware_assignments: info
    unnecessary_null_aware_operator_on_extension_on_nullable: info
    unnecessary_null_in_if_null_operators: info
    unnecessary_nullable_for_final_variable_declarations: info
    unnecessary_overrides: info
    unnecessary_parenthesis: info
    unnecessary_raw_strings: info
    unnecessary_string_escapes: info
    unnecessary_string_interpolations: info
    unnecessary_this: info
    unnecessary_to_list_in_spreads: info
    use_colored_box: info
    use_decorated_box: info
    use_enums: info
    use_full_hex_values_for_flutter_colors: info
    use_function_type_syntax_for_parameters: info
    use_is_even_rather_than_modulo: info
    use_named_constants: info
    use_raw_strings: info
    use_rethrow_when_possible: info
    use_setters_to_change_properties: info
    use_string_buffers: info
    use_string_in_part_of_directives: info
    use_test_throws_matchers: info
    use_to_and_as_if_applicable: info
    void_checks: info
    depend_on_referenced_packages: warning
    package_names: info
    secure_pubspec_urls: info
