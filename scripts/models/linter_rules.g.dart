// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'linter_rules.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LinterRule _$LinterRuleFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const [
      'name',
      'description',
      'details',
      'incompatible',
      'group',
      'state',
      'fixStatus',
      'sinceDartSdk',
      'sets'
    ],
  );
  return LinterRule._(
    name: json['name'] as String,
    description: json['description'] as String,
    group: $enumDecode(_$LintGroupEnumMap, json['group']),
    state: $enumDecode(_$LintStateEnumMap, json['state']),
    sinceDartSdk:
        const SemVerJsonConverter().fromJson(json['sinceDartSdk'] as String),
    incompatible: (json['incompatible'] as List<dynamic>)
        .map((e) => e as String)
        .toList(),
    sets: (json['sets'] as List<dynamic>).map((e) => e as String).toList(),
    hasFix: LinterRule._hasFixFromJson(json['fixStatus'] as String),
    details: json['details'] as String,
  );
}

Map<String, dynamic> _$LinterRuleToJson(LinterRule instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'details': instance.details,
      'incompatible': instance.incompatible,
      'group': _$LintGroupEnumMap[instance.group]!,
      'state': _$LintStateEnumMap[instance.state]!,
      'fixStatus': LinterRule._hasFixToJson(instance.hasFix),
      'sinceDartSdk': const SemVerJsonConverter().toJson(instance.sinceDartSdk),
      'sets': instance.sets,
    };

const _$LintGroupEnumMap = {
  LintGroup.errors: 'errors',
  LintGroup.style: 'style',
  LintGroup.pub: 'pub',
};

const _$LintStateEnumMap = {
  LintState.stable: 'stable',
  LintState.experimental: 'experimental',
  LintState.removed: 'removed',
};
