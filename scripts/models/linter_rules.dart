import "dart:convert";

import "package:json_annotation/json_annotation.dart";
import "package:my_utility/json_converters.dart";
import "package:my_utility/semver.dart";

part 'linter_rules.g.dart';

@JsonSerializable(
  constructor: "_",
  converters: [
    SemVerJsonConverter(),
  ],
)
class LinterRule {
  const LinterRule._({
    required this.name,
    required this.description,
    required this.group,
    required this.state,
    required this.sinceDartSdk,
    required this.incompatible,
    required this.sets,
    required this.hasFix,
    required this.details,
  });

  factory LinterRule.fromJson(Map<String, dynamic> json) {
    final res = _$LinterRuleFromJson(json);
    return LintState.removed == res.state ? RemovedLinterRule._(res) : res;
  }

  /// The name of this rule.
  final String name;

  /// A short description of this rule.
  ///
  /// Also see [details].
  final String description;

  /// A long description of this rule.
  ///
  /// The details are written in markdown.
  ///
  /// Also see [description].
  final String details;

  /// A list of names of other [LinterRule]s that this [LinterRule] is **not
  /// compatible** with.
  final List<String> incompatible;

  /// The group that this rule is part of.
  final LintGroup group;

  /// The state of this rule.
  final LintState state;

  /// Wether or not this rule can be fixed with `dart fix`.
  @JsonKey(
    name: "fixStatus",
    fromJson: _hasFixFromJson,
    toJson: _hasFixToJson,
  )
  final bool hasFix;

  /// The version of the Dart SDK since when this rule is available.
  final SemVer sinceDartSdk;

  final List<String> sets;

  /// The [Uri] that leads to the official Dart API documentation of this
  /// rule.
  Uri get url => getUriFromName(name);

  @override
  int get hashCode => name.hashCode;

  static bool _hasFixFromJson(String json) {
    return switch (json) {
      "hasFix" => true,
      _ => false,
    };
  }

  static String _hasFixToJson(bool value) {
    return value ? "hasFix" : "noFix";
  }

  static Uri getUriFromName(String name) {
    return Uri(
      scheme: "https",
      host: "dart.dev",
      pathSegments: ["tools", "linter-rules", name],
    );
  }

  @override
  bool operator ==(Object other) {
    return other is LinterRule && other.name == name;
  }

  Map<String, dynamic> toJson() => _$LinterRuleToJson(this);

  @override
  String toString() {
    const encoder = JsonEncoder.withIndent("  ");
    return encoder.convert(toJson());
  }
}

// ignore: avoid_implementing_value_types
class RemovedLinterRule implements LinterRule {
  const RemovedLinterRule._(this._rule);

  final LinterRule _rule;

  @override
  String get description => _rule.description;

  @override
  String get details => _rule.details;

  @override
  LintGroup get group => _rule.group;

  @override
  bool get hasFix => _rule.hasFix;

  @override
  List<String> get incompatible => _rule.incompatible;

  @override
  String get name => _rule.name;

  @override
  List<String> get sets => _rule.sets;

  @override
  SemVer get sinceDartSdk => _rule.sinceDartSdk;

  @override
  LintState get state => _rule.state;

  @override
  Uri get url => _rule.url;

  SemVer? get removedSinceDartSdk {
    // If a rule is removed, then the [details] start with a string like this:
    // "NOTE: This rule is removed in Dart 3.3.0;"
    //
    // Some rules, like https://dart.dev/tools/linter-rules/avoid_unstable_final_fields
    // do not have such a note.
    final note = details.split(";").first;
    return SemVer.find(note).firstOrNull;
  }

  @override
  int get hashCode => _rule.hashCode;

  @override
  bool operator ==(Object other) => _rule == other;

  @override
  Map<String, dynamic> toJson() => _rule.toJson();

  @override
  String toString() => _rule.toString();
}

enum LintGroup implements Comparable<LintGroup> {
  //! Do not change the order of the groups
  errors,
  style,
  pub;

  @override
  int compareTo(LintGroup other) => index - other.index;
}

enum LintState {
  stable,
  experimental,
  removed,
}
