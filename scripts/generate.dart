// ignore_for_file: avoid_print

import "dart:convert";
import "dart:io";

import "package:ansi_colorizer/ansi_colorizer.dart";
import "package:http/http.dart" as http;
import "package:my_utility/dartx.dart";
import "package:my_utility/extensions.dart";
import "package:my_utility/io.dart";
import "package:my_utility/printers.dart";
import "package:my_utility/semver.dart";
import "package:yaml/yaml.dart";

import "models/linter_rules.dart";

const _cols = (
  info: AnsiColorizer(foreground: Ansi8BitColor(46)),
  warn: AnsiColorizer(foreground: Ansi8BitColor(208)),
  rule: AnsiColorizer(
    foreground: Ansi24BitColor.fromRGB(150, 220, 255),
    modifiers: {
      AnsiModifier.italic,
    },
  ),
  meta: AnsiColorizer(
    // foreground: Ansi24BitColor.fromRGB(0, 0, 255),
    foreground: Ansi3BitColor(7),
    modifiers: {
      AnsiModifier.faint,
    },
  ),
  desc: AnsiColorizer(
    foreground: Ansi3BitColor(7),
  ),
  detail: AnsiColorizer(
    foreground: Ansi3BitColor(7),
    modifiers: {
      // AnsiModifier.faint,
    },
  ),
  extra: AnsiColorizer(
    foreground: Ansi3BitColor(7),
    modifiers: {
      AnsiModifier.italic,
    },
  ),
  removed: AnsiColorizer(foreground: Ansi24BitColor.fromRGB(255, 0, 0)),
);
const _indent = "\t";
late final _Config _cfg;
late final YamlMap _lastOptions;
late final Directory _rootDir;

Future<void> main() async {
  _cfg = const _Config(showRuleDetails: true);
  // _cfg = const _Config(showRuleDetails: false);

  _rootDir = File.fromUri(Platform.script).parent.parent;
  _lastOptions = _getLastAnalysisOptions(_rootDir);

  final rules = await _getRules();
  final usedRules = <_UsedRule>[];
  for (var i = 0; i < rules.length; i++) {
    final rule = rules[i];

    // Skip the current rule, if it is incompatible with another rule that
    // is already in usage.
    final incompatibleRule = usedRules
        .firstOrNullWhere((e) => e.rule.incompatible.contains(rule.name));
    if (null != incompatibleRule) {
      final tag = _getTag(rules, i);
      printOut(
        "${_cols.warn("$tag INFO:")} Skipping rule ${_cols.rule(rule.name)} "
        "because it is incompatible with the used rule "
        "${_cols.rule(incompatibleRule.rule.name)} "
        "(${_cols.meta(incompatibleRule.rule.url)})\n",
      );
      continue;
    }

    // Check if the rule should be used.
    if (!_promptUseRule(rules, i)) continue;

    usedRules.add(_UsedRule(i, rule, _promptSeverity(rule)));
  }

  _rootDir.file("generated.yaml").writeAsStringSync(
        _createYamlDocument(rules, usedRules),
      );
}

//

String _createYamlDocument(List<LinterRule> rules, List<_UsedRule> used) {
  final buf = IndentedStringBuffer();

  buf.indent = "# ";

  buf.writelnIndented("Created on ${DateTime.now().toUtc().toIso8601String()}");
  buf.writelnIndented();
  buf.writelnIndented("All rules https://dart.dev/tools/linter-rules");
  buf.writelnIndented(
    // ignore: missing_whitespace_between_adjacent_strings
    "Special options https://dart.dev/guides/language/analysis-options"
    "#enabling-additional-type-checks",
  );

  buf.writeln();
  buf.indent = "  ";

  // What rules should be used/not be used
  buf.writeln("linter:");
  buf.writelnIndented("rules:");
  for (final rule in rules) {
    final severity =
        used.firstOrNullWhere((element) => element.rule == rule)?.severity;
    final isUsed = null != severity;

    // Add short description
    for (final l in LineSplitter.split(rule.description)) {
      buf.writelnIndented("# ${l.trim()}", 2);
    }
    buf.writelnIndented("#", 2);

    // Add since when this lint exists
    buf.writelnIndented("# Since Dart SDK: ${rule.sinceDartSdk}", 2);

    // Add the state of this rule
    buf.writelnIndented("# State: ${rule.state.name}", 2);
    // Add since when the rule has been removed
    if (rule is RemovedLinterRule) {
      final since = rule.removedSinceDartSdk ?? "unknown";
      buf.writelnIndented("# Removed since Dart SDK: $since", 2);
    }
    buf.writelnIndented("#", 2);

    // Add which rules this lint is incompatible with
    if (rule.incompatible.isNotEmpty) {
      buf.writelnIndented("# Incompatible with:", 2);
      for (final ruleName in rule.incompatible) {
        buf.writeIndented("#${buf.indent} - $ruleName (", 2);
        buf.write(LinterRule.getUriFromName(ruleName));
        buf.writeln(")");
      }
      buf.writelnIndented("#", 2);
    }

    // Add the url for this rule
    buf.writelnIndented("# ${rule.url}", 2);
    buf.writelnIndented("#", 2);

    // Add the severity for this rule
    if (isUsed) {
      buf.writelnIndented("#+i ${severity.name}", 2);
    } else {
      buf.writelnIndented("#! unused", 2);
    }

    buf.writelnIndented("${rule.name}: $isUsed", 2);

    buf.writeln();
  }

  buf.writeln();

  buf.writeln("analyzer:");

  // What should be excluded from the analyzer
  buf.writelnIndented("exclude:");
  buf.writelnIndented("- .dart_tool/**", 2);
  buf.writelnIndented("# Generated source files", 2);
  buf.writelnIndented("- lib/**.g.dart", 2);
  buf.writelnIndented("# Output from flutter build", 2);
  buf.writelnIndented("- build/**", 2);
  buf.writelnIndented("# Output from https://pub.dev/packages/freezed", 2);
  buf.writelnIndented("- lib/**.freezed.dart", 2);
  buf.writelnIndented("# Output from https://pub.dev/packages/protobuf", 2);
  buf.writelnIndented("- lib/**.pb*.dart", 2);

  buf.writeln();

  // Special rules for the language
  buf.writelnIndented("language:");
  buf.writelnIndented(
    // ignore: missing_whitespace_between_adjacent_strings
    "# See https://dart.dev/guides/language/analysis-options"
    "#enabling-additional-type-checks",
    2,
  );
  buf.writelnIndented("strict-casts: false", 2);
  buf.writelnIndented("strict-inference: false", 2);
  buf.writelnIndented("strict-raw-types: true", 2);

  buf.writeln();

  // The severity of the rules
  buf.writelnIndented("errors:");

  // special rules
  buf.writelnIndented(
    "# Special rules that are not documented on "
    "https://dart.dev/tools/linter-rules",
    2,
  );
  buf.writelnIndented("todo: info", 2);
  buf.writelnIndented("dead_code: info", 2);
  buf.writelnIndented("invalid_assignment: error", 2);
  buf.writelnIndented("missing_required_param: error", 2);
  buf.writelnIndented("missing_return: error", 2);
  buf.writelnIndented("import_of_legacy_library_into_null_safe: error", 2);
  buf.writelnIndented("invalid_use_of_visible_for_testing_member: warning", 2);

  buf.writeln();

  // all other rules

  buf.writelnIndented("# All used rules", 2);
  for (final item in used) {
    final _UsedRule(index: _, rule: rule, severity: severity) = item;
    buf.writelnIndented("${rule.name}: ${severity.name}", 2);
  }

  return buf.toString();
}

_Severity? _tryParseSeverity(dynamic value) {
  return _Severity.values.firstOrNullWhere((e) => e.name == value);
}

YamlMap _getLastAnalysisOptions(Directory root) {
  final filenameRegex = RegExp(r"analysis_options_(\d+-\d+-\d+)\.yaml");
  final filesWithVersion = root
      .directory("lib")
      .listSync()
      .map(
        (e) {
          final rawVersion =
              filenameRegex.firstMatch(e.name)?.group(1)?.replaceAll("-", ".");

          if (null == rawVersion) return null;

          return (e as File, SemVer.parse(rawVersion));
        },
      )
      .whereNotNull()
      .sortedBy((element) => element.$2)
      .toList();

  final lastAnalysisFile = filesWithVersion.last.$1;

  return loadYamlNode(lastAnalysisFile.readAsStringSync()) as YamlMap;
}

Future<List<LinterRule>> _getRules() async {
  /// Source of truth for linter rules.
  ///
  /// See https://github.com/dart-lang/lints/blob/baaaa5616370243c3c8c6a9f67fb83d26bdac552/tool/gen_docs.dart#L12C1-L15C1
  const rulesUrl =
      "https://raw.githubusercontent.com/dart-lang/site-www/main/src/_data/linter_rules.json";

  final res = await http.get(Uri.parse(rulesUrl));
  if (HttpStatus.ok != res.statusCode) {
    printError("Failed to retrieve the linter urls");
    exit(1);
  }

  final rules = (jsonDecode(res.body) as List<dynamic>)
      // ignore: unnecessary_lambdas
      .map((e) => LinterRule.fromJson(e))
      .toList();

  rules.sort(
    (a, b) => switch (a.group.compareTo(b.group)) {
      0 => a.name.compareTo(b.name),
      final val => val,
    },
  );

  return rules;
}

String _getTag(List<LinterRule> rules, int index) {
  return "[${index + 1}/${rules.length}]";
}

bool _promptUseRule(List<LinterRule> rules, int index) {
  final tag = _getTag(rules, index);
  final rule = rules[index];

  final msg = IndentedStringBuffer(indent: _indent);
  msg.writeln("$tag: ${_cols.rule(rule.name)}");
  msg.writeln();
  msg.writelnIndented(
    _cols.meta("- Since Dart SDK ${rule.sinceDartSdk}"),
  );
  msg.writelnIndented(_cols.meta("- ${rule.url}"));

  final wasUsedInLastOptions =
      // ignore: avoid_dynamic_calls
      (_lastOptions["linter"]["rules"] as YamlMap).getOrElse(
    rule.name,
    () => false,
  );

  switch (rule.state) {
    case LintState.stable:
      printOut(msg, "");
      stdin.readLineSync();
      return _promptUseStableRule(
        IndentedStringBuffer(),
        rule,
        wasUsedInLastOptions,
      );

    case LintState.experimental:
      msg.writelnIndented(_cols.warn("- EXPERIMENTAL"));
      printOut(msg, "");
      stdin.readLineSync();
      return _promptUseExperimentalRule(
        IndentedStringBuffer(),
        rule,
        wasUsedInLastOptions,
      );

    case LintState.removed:
      return _promptUseRemovedRule(msg, rule);
  }
}

bool _promptUseStableRule(
  IndentedStringBuffer buf,
  LinterRule rule,
  bool useByDefault,
) {
  if (!_cfg.showRuleDetails) {
    buf.writelnIndented(_cols.desc(rule.description));
  }

  if (_cfg.showRuleDetails) {
    buf.writeln();
    buf.writeln(
      _cols.detail(
        rule.details.split("\n").joinToString(
              separator: "\n",
              transform: (e) => "$_indent$e",
            ),
      ),
    );
  }

  buf.write(_cols.extra("${_indent}Use this rule ? "));
  if (useByDefault) {
    buf.writeln("[Y/n]");
  } else {
    buf.writeln("[y/N]");
  }

  printOut(buf);

  return switch (stdin.readLineSync()?.toLowerCase().trim()) {
    "y" => true,
    "n" => false,
    _ => useByDefault,
  };
}

bool _promptUseExperimentalRule(
  IndentedStringBuffer buf,
  LinterRule rule,
  bool useByDefault,
) {
  return _promptUseStableRule(buf, rule, useByDefault);
}

bool _promptUseRemovedRule(IndentedStringBuffer buf, LinterRule rule) {
  buf.writelnIndented(_cols.removed("This rule has already been removed,"));
  buf.writelnIndented(_cols.removed("do you still want to use it ? [y/N]"));

  return switch (stdin.readLineSync()?.toLowerCase().trim()) {
    "y" => true,
    _ => false,
  };
}

_Severity _promptSeverity(LinterRule rule) {
  final defaultSeverity = _tryParseSeverity(
    // ignore: avoid_dynamic_calls
    _lastOptions["analyzer"]["errors"][rule.name],
  );

  return promptUntilValidPick(
    prompt: _cols.extra("How should the rule be treated ?"),
    defaultValue: defaultSeverity,
    options: _Severity.values.toSet(),
    stringify: (index, option) {
      final col = _cols.extra.copyWith(
        foreground: switch (option) {
          _Severity.info => Ansi3BitColors.cyan,
          _Severity.warning => const Ansi8BitColor(208),
          _Severity.error => Ansi3BitColors.brightRed,
        },
      );

      var str = col(option.name);
      if (defaultSeverity == option) {
        str += " ${_cols.extra("(default)")}";
      }

      return (
        selector: (index + 1).toString(),
        option: str,
      );
    },
    onError: (input) => _cols.extra(
      input.isBlank
          ? "You must pick an option"
          : "There is no option for the value '$input'",
    ),
    selectorFormatter: _cols.extra,
    separator: _cols.extra(")"),
    promptIndent: _indent,
    errorIndent: _indent,
    optionsIndent: _indent * 2,
  );
}

//

final class _Config {
  const _Config({
    required this.showRuleDetails,
  });

  final bool showRuleDetails;
}

final class _UsedRule {
  const _UsedRule(this.index, this.rule, this.severity);

  final int index;
  final LinterRule rule;
  final _Severity severity;
}

enum _Severity {
  //! Do not change this order
  info,
  warning,
  error,
}
