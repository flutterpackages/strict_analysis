# 2.0.0

  * Moved all packages from `dependencies` to `dev_dependencies` in pubspec.yaml

  * Increased min Dart SDK version constraints to `>=3.4.0 <4.0.0`

  * Added generator script to easily create new linting options

# 1.0.3

  * Increased min Dart SDK version constraints to `>=3.0.0 <4.0.0`

  * Added `deprecated_member_use_from_same_package` and set it to `true`

  * Added experimental `implicit_reopen` and set it to `true`

  * Added experimental `invalid_case_patterns` and set it to `true`

  * Commented `list_remove_unrelated_type` as it became deprecated

  * Added `use_build_context_synchronously` and set it to `true`

  * Added `matching_super_parameters` and set it to `true`

  * Added experimental `no_default_cases` and set it to `true`

  * Added experimental `no_literal_bool_comparisons` and set it to `true`

  * Added `type_literal_in_constant_pattern` and set it to `true`

  * Added `unnecessary_breaks` and set it to `false`

  * Added `unnecessary_null_checks` and set it to `true`

  * Added `unreachable_from_main` and set it to `true`

  * Added `use_late_for_private_fields_and_variables` and set it to `true`

  * Added experimental`use_super_parameters` and set it to `true`

# 1.0.2

  * Set `strict-inference` to `false`

  * Set `strict-raw-types` to `true`

  * Disabled `discarded_futures`

  * Disabled `no_leading_underscores_for_local_identifiers`

# 1.0.1

  Included new lints from https://dart-lang.github.io/linter/lints/index.html

# 1.0.0

  Initial version.
